import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:efrei2023gr3/constante.dart';
import 'package:efrei2023gr3/controller/firestoreHelper.dart';
import 'package:efrei2023gr3/model/my_user.dart';
import 'package:efrei2023gr3/view/my_chats.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class MyListePersonne extends StatefulWidget {
  const MyListePersonne({super.key});

  @override
  State<MyListePersonne> createState() => _MyListePersonneState();
}

class _MyListePersonneState extends State<MyListePersonne> {
   MyUser? currentUser;
  @override
  void initState() {
    super.initState();
    User? user = FirestoreHelper().auth.currentUser;
    if (user != null) {
      FirestoreHelper().cloudUsers.doc(user.uid).get().then((DocumentSnapshot documentSnapshot) {
        if (documentSnapshot.exists) {
          setState(() {
            currentUser = MyUser.dataBase(documentSnapshot);
          });
        }
      });
    }
  }

  Widget build(BuildContext context) {
    //obtenir l'utilisateur connecté

    return StreamBuilder<QuerySnapshot>(
        stream: FirestoreHelper().cloudUsers.snapshots(),
        builder: (context, snap){
          if(ConnectionState.waiting == snap.connectionState){
            return const CircularProgressIndicator.adaptive();
          }
          else
            {
              if(snap.hasData == null){
                return const Text("Aucune donnée");
              }
              else
                {
                  List documents = snap.data!.docs;
                  return ListView.builder(
                    itemCount: documents.length,
                      itemBuilder: (context,index){
                       MyUser otherUser = MyUser.dataBase(documents[index]);
                       if(Moi.uid == otherUser.uid){
                         return Container();
                       }
                       else {
                         bool isFavorite = currentUser?.favoris?.contains(otherUser.uid) ?? false;
                         return Dismissible(
                             key: Key(otherUser.uid),
                           background: Container(
                             color: Colors.red,
                           ),
                             child: Card(
                               elevation: 5,
                               color: Colors.amber,
                               shape: RoundedRectangleBorder(
                                   borderRadius: BorderRadius.circular(15)),
                               child: ListTile(
                                 onTap: (){
                                   Navigator.push(context, MaterialPageRoute(builder: (context){
                                     return MyChat(user: otherUser);
                                   }
                                   ));
                                 },
                                 leading: CircleAvatar(
                                   radius: 70,
                                   backgroundImage: NetworkImage(otherUser.avatar ??
                                       imageDefault),
                                 ),
                                 title: Text(otherUser.fullName),
                                 subtitle: Text(otherUser.email),
                                 trailing: IconButton(
                                   icon: Icon(
                                     isFavorite ? Icons.favorite: Icons.favorite_border,
                                     color: isFavorite ? Colors.red:null,
                                   ),
                                   onPressed: () async{
                                     setState(() {
                                       if (isFavorite){
                                         currentUser?.favoris?.remove(otherUser.uid);
                                         FirestoreHelper().removeFavoris(currentUser!.uid, otherUser.uid);
                                       }else{
                                         currentUser?.favoris?.add(otherUser.uid);
                                         FirestoreHelper().addFavoris(currentUser!.uid, otherUser.uid);
                                       }
                                     });
                                   },
                                 ),
                               ),
                             ),
                             );
                           }
                          }
                      );
                    }

                }
        }
    );
  }
}
