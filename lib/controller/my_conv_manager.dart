import 'package:efrei2023gr3/model/message.dart';
class ConversationManager {
  List<Message> messages = [];

  // Ajoute un message à la conversation
  void addMessage(Message message) {
    messages.add(message);
  }
}