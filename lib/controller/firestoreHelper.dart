//fichier qui gère les opérations sur la base de donnée

import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:efrei2023gr3/model/my_user.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';

import '../model/message.dart';

class FirestoreHelper {
  final auth = FirebaseAuth.instance;
  final cloudUsers = FirebaseFirestore.instance.collection("UTILISATEURS");
  final cloudMessage = FirebaseFirestore.instance.collection("MESSAGES");
  final storage = FirebaseStorage.instance;

  //création d'un utilisateur
  Future<MyUser>RegisterMyUser(String nom , String prenom , String email, String password) async {
    UserCredential credential = await auth.createUserWithEmailAndPassword(email: email, password: password);
    String uid = credential.user!.uid;
    Map<String,dynamic> map = {
      "NOM": nom,
      "PRENOM":prenom,
      "EMAIL": email,
    };
    addUser(uid,map);
    return getUser(uid);


  }

  //récuperer les infos de l'utilisateur
  Future<MyUser> getUser(String uid) async{
    DocumentSnapshot snapshot = await cloudUsers.doc(uid).get();
    return MyUser.dataBase(snapshot);
  }
  //Afficher les discussions d'un utilisateur
  Stream<List<MyUser>> get getDiscussionUser {
    return cloudUsers
        .where('uid', isNotEqualTo: FirestoreHelper().user.uid)
        .snapshots()
        .map((event) =>
        event.docs.map((e) => MyUser.fromJson(e.data())).toList());
  }

  // Récupérer les messages
  Stream<List<Message>> getMessage(String receiverUID, [bool myMessage = true]) {
    return cloudMessage
        .where('senderUID', isEqualTo: myMessage ? FirestoreHelper().user.uid : receiverUID)
        .where('receiverUID', isEqualTo: myMessage ? receiverUID : FirestoreHelper().user.uid)
        .snapshots()
        .map((event) => event.docs.map((e) => Message.fromJson(e.data(), e.id)).toList());
  }

  //Envoi de messages
  Future<bool> sendMesssage(Message msg) async {
    try {
      await cloudMessage.doc().set(msg.toJson());
      return true;
    }
    catch (e) {
      return false;
    }
  }

  //ajouter dans la bdd les infos utilisateurs
  addUser(String uid, Map<String,dynamic> data){
    cloudUsers.doc(uid).set(data);
  }


  //connexion d'un utilisateur
  Future<MyUser>ConnectMyUser(String email, String password) async{
    UserCredential credential = await auth.signInWithEmailAndPassword(email: email, password: password);
    String uid = credential.user!.uid;
    return getUser(uid);

  }

  //Déconnexion d'un utilisateur
  SignOut () async {
    try {
      await auth.signOut();
    }
    catch (e) { }
  }


  User get user => FirebaseAuth.instance.currentUser!;
  Stream<User?> get onChangeUser => auth.authStateChanges();


  //suppression d'un utilisateur
  DeleteAccount(MyUser user) async {
    try {
      if(FirebaseAuth.instance.currentUser!.uid == user.uid) {
        await cloudUsers.doc(user.uid).delete();
        await FirebaseAuth.instance.currentUser!.delete();
      }
    }
    catch (e) {
      print('Erreur lors de la suppression de l\'utilisateur : $e');
    }
  }
  //mise à jour d'un utilisateur
  updateUser(String uid, Map<String,dynamic> data){
    cloudUsers.doc(uid).update(data);
  }

  //Ajout des favoris
  Future<void> addFavoris(String userId, String favoriId) async {
    try {
      DocumentReference userDoc = cloudUsers.doc(userId);
      await userDoc.update({
        'favoris': FieldValue.arrayUnion([favoriId])
      });
    } catch (e) {
      print("Erreur lors de l'ajout aux favoris : $e");
    }
  }

  //Suppression des favoris
  Future<void> removeFavoris(String userId, String favoriId) async {
    try {
      DocumentReference userDoc = cloudUsers.doc(userId);
      await userDoc.update({
        'favoris': FieldValue.arrayRemove([favoriId])
      });
    } catch (e) {
      print("Erreur lors de la suppression des favoris : $e");
    }
  }

  //stockage image
  Future<String>stockageFiles(String nameImage,Uint8List bytesImage,String dossier,String uid) async {
    TaskSnapshot snapshot = await storage.ref("/$dossier/$uid/$nameImage").putData(bytesImage);
    String url = await snapshot.ref.getDownloadURL();
    return url;

  }
}
