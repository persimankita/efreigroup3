
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:efrei2023gr3/constante.dart';
import 'package:efrei2023gr3/model/message.dart';

import '../controller/firestoreHelper.dart';

class MyUser {
  //attributs
  late String uid;
  late String nom;
  late String prenom;
  late String email;
  String? avatar;
  List? favoris;

  //variable calculé
  String get fullName {
    return prenom + " " + nom;
  }


  //constructeurs
  MyUser(){
    uid = "";
    nom = "";
    prenom = "";
    email = "";
  }
  MyUser.dataBase(DocumentSnapshot snapshot){
    uid = snapshot.id;
    Map<String,dynamic> data = snapshot.data() as Map<String,dynamic>;
    nom = data["NOM"];
    prenom = data["PRENOM"];
    email = data["EMAIL"];
    favoris = data["FAVORIS"]??[];
    avatar = data["AVATAR"]??imageDefault;
  }

  MyUser.fromJson(Map<String, dynamic> json) {
    uid = json['uid'];
    nom = json['nom'];
    prenom = json['prenom'];
    email = json['email'];
    avatar = json['avatar'];
    favoris = json['favoris'];
  }
}